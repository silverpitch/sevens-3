<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player', function(Blueprint $table) {
            $table->increments('id');
            $table->string('fname');
            $table->string('lname');
            $table->integer('teams_id')->unsigned();;
            $table->foreign('teams_id')->references('id')->on('teams');
            $table->integer('weight');
            $table->integer('height');
            $table->float('fitness');
            $table->float('stamina');
            $table->float('handling');
            $table->float('attack');
            $table->float('defense');
            $table->float('strength');
            $table->float('jumping');
            $table->float('speed');
            $table->float('technique');
            $table->float('agility');
            $table->float('kicking');
            $table->float('form');
            $table->float('aggression');
            $table->float('discipline');
            $table->float('energy');
            $table->float('leadership');
            $table->float('experience');
            $table->float('injured');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('player');
    }
}
