<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams',
        function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->timestamps();
            $table->string('name',100);
            $table->string('team_type',20);
            $table->string('manager',100);
            $table->decimal('ranking_points')->default(40.00);

        });
    }

    public function down()
    {
        Schema::drop('teams');
    }
}
