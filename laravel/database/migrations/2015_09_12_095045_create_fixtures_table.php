<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFixturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fixtures', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('home_team');
            $table->integer('guest_team');
            $table->string('type');
            $table->string('team_one_attack');
            $table->string('team_one_defense');
            $table->string('team_two_attack');
            $table->string('team_two_defense');
            $table->boolean('played');
            $table->integer('p1t1');
            $table->integer('p2t1');
            $table->integer('p3t1');
            $table->integer('p4t1');
            $table->integer('p5t1');
            $table->integer('p6t1');
            $table->integer('p7t1');
            $table->integer('p8t1');
            $table->integer('p9t1');
            $table->integer('p10t1');
            $table->integer('p11t1');
            $table->integer('p12t1');
            $table->integer('p1t2');
            $table->integer('p2t2');
            $table->integer('p3t2');
            $table->integer('p4t2');
            $table->integer('p5t2');
            $table->integer('p6t2');
            $table->integer('p7t2');
            $table->integer('p8t2');
            $table->integer('p9t2');
            $table->integer('p10t2');
            $table->integer('p11t2');
            $table->integer('p12t2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fixtures');
    }
}
