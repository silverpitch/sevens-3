<!DOCTYPE html>
<html>
    <head>
        <title>Silver Pitch Sevens 3</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="{{ asset('dist/semantic.min.css')}}">
        <script src="{{ asset('dist/semantic.min.js') }}"></script>
    </head>
    <body>
        <div class="ui container">
            <h1 align="center">Welcome to Silver Pitch sevens 3. </h1>
            <br>

            <div align="right"><b>Debug Console</b></div>
            <hr />
            <div class="ui grid">
                <div class="four wide column">
                   jhk
                </div>
                <div class="four wide column">jkh</div>
                <div class="four wide column">jhkj</div>
                <div class="four wide column">khj</div>
            </div>
        </div>
    </body>
</html>
