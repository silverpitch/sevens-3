<!DOCTYPE html>
<html>
<head>
    <title>Silver Pitch Sevens 2.1</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <script type="text/javascript" src="js/TypingText.js">
</script>
</head>
<body>
<br>
<div class="container">
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="live.php">Silver Pitch Sevens</a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
            <li><a href="index.php">Home</a></li>
            <li class="active"><a href="live.php">Live Game</a></li>
        </ul>
    </div>
</nav>
<br>
<br>
<div align="center">
<div id="example11">

<?php
set_time_limit(0);
include('minor_calls/minor_game_functions.php');
include('database/db_driver.php');
include('post_game/energy_functions.php');
include('post_game/in_game_training.php');

//include('commentary_functions.php');
connect();

//look for matches not played
$result = mysql_query("SELECT `id` FROM `fixtures` WHERE `played`=0");
$matches = array();

$number_of_matches=mysql_num_rows($result);
print $number_of_matches;

while($row=mysql_fetch_assoc($result)){
$matches[] = $row['id'];
}
//start simulation looping though all the matches
foreach($matches as $match)
{

	$match_id = $match;
	//basic variables
	$minutes = 420;
	//for first half
	$team    = array();
	$weather = "sunny";
	//this is where id do the fetch from the db
	$players = array();
	$players[1] = get_home_team_players($match_id);
	$players[2] = get_guest_team_players($match_id);
	$team[1] = $players[1][7];
	// index 7 is where the team name is
	$team[2] = $players[2][7];
	// same to above
	//tactics section
	$energy = array();
	$tendancy_to_pass[] = array();
	$ruck_commit[]      = array();
	$attack_method[]    = array();
	$defence_method[]   = array();
	//team one tactics
	$attack_method[1]  = $players[1][8];
	$defence_method[1] = $players[1][9];
	$energy[1] = 1;
	//team two tactics
	$attack_method[2]  =$players[2][8];
	$defence_method[2] = $players[2][9];
	$energy[2]= 1;
	//initialise empty variables to be used as statistics holders
	$points  = array();
	$points[1][1] = 0;
	$points[2][1] = 0;
	$points[1][0] = 0;
	$points[2][0] = 0;
	$try[] = array();
	$try[1] = 0;
	$try[2] = 0;
	//mauls
	$mauls_won = array();
	$mauls_won[1] = 0;
	$mauls_won[2] = 0;
	//rucks
	$rucks_won = array();
	$rucks_won[1] = 0;
	$rucks_won[2] = 0;
	$passes_made = array();
	$tackles_made = array();
	$linebreaks = array();
	//scrums
	$scrums_won = array();
	$scrums_won[1] = 0;
	$scrums_won[2] = 0;
	//get team one players from the database
	for($i=0;$i<=6;$i++)
	{
		$player[1][$i] = get_player_details($team[1], $players[1][$i]);
	}

	//get team two players from the database
	for($i=0;$i<=6;$i++)
	{
		$player[2][$i] = get_player_details($team[2], $players[2][$i]);
	}

	//get the team name from the team table
	$team[1] = get_teamname($team[1]);
	$team[2] = get_teamname($team[2]);
	set_tactics();
	//*******************************************************************************
	//display of players and teams
	echo "<div  style=\"float:left; width: 50%; color:purple\">";
	echo "<h3 class=\"silver\" ><b>" . strtoupper($team[1]) . "</b></h3>";
	for ($i = 0; $i <= 6; $i++)
	{
		$jersey_number = $i + 1;
		echo $jersey_number . ". <span class=\"silver\" float=\"right \">" . $player[1][$i][0] . " ".$player[1][$i][15]."<br></span><br>";
	}

	echo "</div>";
	echo "<div style=\"float:right; width: 50%; color:grey\">";
	echo "<h3 class=\"silver\"><b>" . strtoupper($team[2]) . "</b></h3>";
	for ($i = 0; $i <= 6; $i++)
	{
		$de = $i + 1;
		echo $de . ". <span class=\"silver\" float=\"left\" >" . $player[2][$i][0] ." ".$player[2][$i][15]. "<br></span><br>";
	}

	echo "</div><p><br><div style=\"clear:both;\">";
	//*******************************************************************************
	//match formatting and all
	for ($i=0; $i<=6; $i++)
	{
		$de = $i+1;
		$player[1][$i][0] = "<span style=\"color:purple\"> <b>($de) .".$player[1][$i][0] ." ".$player[1][$i][15]."</b></span>";
		$player[2][$i][0] = "<span style=\"color:#666633\"> <b>($de) .".$player[2][$i][0]." ".$player[2][$i][15]."</b></span>";
	}

	echo "<hr />";
	//*******************************************************************************
	$ground = 50;
	show_time();
	/* the fourth player is usually the scrum half hence takes the kick at the start
arrays begin with index 0 so player 1 is index 0, player 4 will be index 3 */
	$starting_team = mt_rand(1, 2);
	//decides who starts the game by toss of a coin
	
	if ($starting_team == 1)
	{
		/* index 0 refers to the players name. Variable ball will hold the name of who has the ball,
		Variable current will hold whoever starts the match
		( often the team which has the ball, either one or two ) */
		$current  = 1;
		$opponent = 2;
		$ball     = $player[$current][3][0];
		$number   = 3;
		$starting_team_inverse = 2;
	}
	else
	{
		$opponent = 1;
		$current  = 2;
		$number   = 3;
		$ball     = $player[$current][3][0];
		$starting_team_inverse = 1;
	}

	//some basic formatting for commentary
	$team[1] = "<span style=\"color:purple\"> <b>".$team[1]."</b> </span>";
	$team[2] = "<span style=\"color:#666633\"> <b>".$team[2]."</b> </span>";
	//------------------------------------------------------------------------------------------------------------------------------
	//------------------------------------------------------------------------------------------------------------------------------
	//officially start the match now
	save_commentary("<b>" . $ball . "</b> from ".$team[$current]." gets to start the match! ");
	list($result, $gain, $lineout,$kicked_side) = kickoff();
	// the ball is kicked
	//line-out variable is to be used in later versions
	after_kick();
	//proper adjustment after kick e.g. possession,global variables are also updated. Determines what happens next
	$half = 1;
	for ($i=0; $i<=100; $i++)
	{
		stimulate_game();
		echo "<hr />";
		
		if ($minutes <= 0)
		{
			save_commentary(", but the ref blows the whistle for the first half to end, the score standing at <p>".$team[1]." ".$try[1]." - ".$try[2]." ".$team[2]);
			save_commentary("<br><br><img src=\"img/stadium.jpg\" width=\"800\" height=\"600\"><br><br><h1><b>$try[1] - Half-Time - $try[2]</b></h1><br><br>");
			break;
		}
		else
		{
			show_time();
		}

	}

	//------------------------------------------------------------------------------------------------------------------------------
	//------------------------------------------------------------------------------------------------------------------------------
	//first time half is over re-instiate some of the variables
	$minutes = 420;
	$half = 2;
	$ground = 50;
	$number = 4;
	$current = $starting_team_inverse;
	save_commentary(" Welcome back to the second half folks, <b>" . $player[$current][$number][0] . "</b> from ".$team[$current]." will get the match under-way for us! ");
	//start the second half
	list($result, $gain, $lineout) = kickoff();
	after_kick();
	for ($i=0; $i<=110; $i++)
	{
		'<p>'.stimulate_game().'</p>';
		echo "<hr />";
		
		if ($minutes <= 0)
		{
			save_commentary(". The siren for the last play blows, the match is coming to an end.");
			stimulate_game();
			
			if ($try[1]==$try[2])
			{
				save_commentary(", but thats it, thats the end of the match, ".$team[1]." and ".$team[2]. " walk away with a draw. ");
			}
			else
			{
				
				if ($try[1]>$try[2])
				{
					$winner = $team[1];
				}

				elseif ($try[1]<$try[2])
				{
					$winner = $team[2];
				}

				save_commentary(", but thats it, thats the end of the match, ".$winner. " walk away with a win, a match well played for them.");
				save_commentary("<br><h3>Full-Time</h3>");
			}

			break;
		}
		else
		{
			show_time();
		}

	}

	save_commentary("
			<hr /><br> <b><h4>".strtoupper($team[1])." ".$try[1]." - ".$try[2]." ".strtoupper($team[2])."
			</b></h4>");
	//------------------------------------------------------------------------------------------------------------------------------
	//post game
	echo $player[1][0][4].'--';
	apply_igt();
	echo $player[1][0][4].'--';
	energy_adjustment();
	for($i=0;$i<=6;$i++)
	{
		update_players($player[1][$i][18],1,$i);
		update_players($player[2][$i][18],2,$i);
	}

	echo "<br>";
	$errors01 = 0;
	$errors02 = 0;
	for ($i=0; $i<=6; $i++)
	{
		$errors01 +=  $player[1][$i][14];
	}

	for ($i=0; $i<=6; $i++)
	{
		$errors02 +=  $player[2][$i][14];
	}

	$stat_tries1 = 0;
	$stat_tries2 = 0;
	for ($i=0; $i<=6; $i++)
	{
		
		if (!$player[1][$i][13]==0)
		{
			$stat_tries1 += $player[1][$i][13];
		}

	}

	for ($i=0; $i<=6; $i++)
	{
		
		if (!$player[2][$i][13]==0)
		{
			$stat_tries2 += $player[2][$i][13];
		}

	}

	$total_points_one = $points[1][1] * 2;
	$total_points_two = $points[2][1] * 2;
	$total_kicks_one = $points[1][1] + $points[1][0];
	$total_kicks_two = $points[2][1] + $points[2][0];
	save_statistics();
}

//------------------------------------------------------------------------------------------------------------------------------
?>
</div>
<br>
<br>
<br>
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
    View Match Statistics
</button>
<br>
<br>
<br>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>          </button>
    <h4 class="modal-title" id="myModalLabel"><h2>Match Statistics.</h2</h4>
</div>
<div class="modal-body">
<?php
	//post match statistics, not that important anyway
	//----------------------------------------------------------------------------------------------------------
	echo "<br>";
	$errors01 = 0;
	$errors02 = 0;
	echo "<div class=\"row\">
<div class=\"col-md-6\">
";
	echo '<b>'.$team[1]." Errors</b><br>";
	for ($i=0; $i<=6; $i++)
	{
		echo $player[1][$i][0].' had '.$player[1][$i][14].' errors.<br> ';
		$errors01 +=  $player[1][$i][14];
	}

	echo "</div>";
	echo "
<div class=\"col-md-6\">
";
	echo '<b>'.$team[2]." Errors</b><br>";
	for ($i=0; $i<=6; $i++)
	{
		echo $player[2][$i][0].' had '.$player[2][$i][14].' errors.<br> ';
		$errors02 +=  $player[2][$i][14];
	}

	echo "</div>";
	echo '<br><br>';
	echo '<b><br><u>'.$team[1]."Try Scoreres</b></u><br>";
	$stat_tries1 = 0;
	$stat_tries2 = 0;
	for ($i=0; $i<=6; $i++)
	{
		
		if (!$player[1][$i][13]==0)
		{
			echo $player[1][$i][0].'<b>( '.$player[1][$i][13].')</b><br>';
			$stat_tries1 += $player[1][$i][13];
		}

	}

	echo '<b><br><u>'.$team[2]."Try Scoreres</b></u><br>";
	for ($i=0; $i<=6; $i++)
	{
		
		if (!$player[2][$i][13]==0)
		{
			echo $player[2][$i][0].'<b>( '.$player[2][$i][13].')</b><br>';
			$stat_tries2 += $player[2][$i][13];
		}

	}

	echo '<br><br>';
	echo "<b><br><u>Set Pieces</u></b><br>";
	echo '<b>'.$team[1]." scrums won: ".$scrums_won[1]."</b><br>";
	echo '<b>'.$team[2]." scrums won: ".$scrums_won[2]."</b><br>";
	echo "<b><br><u>Mauls Won</u></b><br>";
	echo '<b>'.$team[1]." mauls won: ".$mauls_won[1]."</b><br>";
	echo '<b>'.$team[2]." mauls won: ".$mauls_won[2]."</b><br>";
	echo "<b><br><u>Rucks Won</u></b><br>";
	echo '<b>'.$team[1]." rucks won: ".$rucks_won[1]."</b><br>";
	echo '<b>'.$team[2]." rucks won: ".$rucks_won[2]."</b><br>";
	echo '<br><br>';
	$total_points_one = $points[1][1] * 2;
	$total_points_two = $points[2][1] * 2;
	$total_kicks_one = $points[1][1] + $points[1][0];
	$total_kicks_two = $points[2][1] + $points[2][0];
	echo '<b>'.$team[1]." Conversions</b><br>";
	echo "Successful Kicks = ".$points[1][1].'<br>';
	echo "Failed Kicks = ".$points[1][0].'<br>';
	echo "Total Points from Kicks = ".$total_points_one.'<br>';
	echo '<b>'.$team[2]." Conversions</b><br>";
	echo "Successful Kicks = ".$points[2][1].'<br>';
	echo "Failed Kicks = ".$points[2][0].'<br>';
	echo "Total Points from Kicks = ".$total_points_two.'<br>';
	save_statistics();
	?>
       </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div> 
  </div>
<p id="example2"></p>
<script type="text/javascript">
//Define first typing example:
new TypingText(document.getElementById("example1"));
//Define second typing example (use "slashing" cursor at the end):
new TypingText(document.getElementById("example2"), 3000, function(i){ var ar = new Array("\\", "|", "/", "-"); return " " + ar[i.length % ar.length]; });
//Type out examples:
TypingText.runAll();
</script>
	<!-- ********************************************************************************************** -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>