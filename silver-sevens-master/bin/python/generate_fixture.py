from datetime import date
from peewee import *
from db_tables.tables import Team,Player, Fixtures
from libs.general import rand
from array import array
import random
import sys

#for purposes of fast development, I will assume the following teams are in the database.
team_one = raw_input('Enter the name of the First Team: ')

team_two = raw_input('Enter the name of the Second Team: ')


#Query team from database just to check if its there
try:
    team_one = Team.get(name = team_one)
    team_two =  Team.get(name = team_two)
except Exception, e:
    print "An error occurred. Most likely the one of the teams selected do not exist."

teamone_players = []
teamtwo_players = []
for person in Player.select().where(Player.team == team_one).order_by(Player.fitness):
    teamone_players.append(person.id)

for person in Player.select().where(Player.team == team_two).order_by(Player.fitness):
    teamtwo_players.append(person.id)

#the next block of code sets the tactics too
attack = ['technical','aggressive']
defense = ['wide','group']
one_attack = random.choice(attack)
one_defense = random.choice(defense)

attack = ['technical','aggressive']
defense = ['wide','group']
two_attack = random.choice(attack)
two_defense = random.choice(defense)

new_fixture = Fixtures.create(home_team = team_one,
                            guest_team = team_two,
                            competition = "Friendly",
                            stage = "Friendly",
                            played = False,
                            team_one_attack = one_attack,
                            team_one_defense = one_defense,
                            team_two_attack = two_attack,
                            team_two_defense = two_defense,
                            p1t1 = teamone_players[1],
                            p2t1 = teamone_players[2],
                            p3t1 = teamone_players[3],
                            p4t1 = teamone_players[4],
                            p5t1 = teamone_players[0],
                            p6t1 = teamone_players[6],
                            p7t1 = teamone_players[7],
                            p8t1 = teamone_players[8],
                            p9t1 = teamone_players[9],
                            p10t1 = teamone_players[10],
                            p11t1 = teamone_players[11],
                            p12t1 = teamone_players[5],
                            p1t2 = teamtwo_players[1],
                            p2t2 = teamtwo_players[2],
                            p3t2 = teamtwo_players[3],
                            p4t2 = teamtwo_players[0],
                            p5t2 = teamtwo_players[5],
                            p6t2 = teamtwo_players[6],
                            p7t2 = teamtwo_players[7],
                            p8t2 = teamtwo_players[8],
                            p9t2 = teamtwo_players[9],
                            p10t2 = teamtwo_players[10],
                            p11t2 = teamtwo_players[11],
                            p12t2 = teamtwo_players[4],
                        )
dir(new_fixture)
print "The fixture:"+team_one.name+" vs "+team_two.name+" was successful created."
raw_input()
