from datetime import date
from peewee import *
from db_tables.tables import Team,Player
from libs.general import rand
import random
from array import array

while(range(1,5)):
	team_name = raw_input("Team Name:")

	#division - just allows generation of skills to vary according to division

	division = rand(1,5)
	if (division==1):
		max_seed = 15
		min_seed = 11
	elif (division==2):
		max_seed = 13
		min_seed = 9
	elif (division==3):
		max_seed = 11
		min_seed = 7
	elif (division==4):
		max_seed = 9
		min_seed = 5
	elif (division==5):
		max_seed = 7
		min_seed = 3
	else:
		print "Invalid division."			

	team = Team.create(name = team_name,location= 'Nairobi')

	player = {}

	for i in range(0,12):
		i += 1
		first_name = random.choice(["Peter",            "John",            "Mathew",            "Mark",            "Anthony",            "Andrew",            "Edward",            "Wilson",            "Levy",            "Letser",            "George",            "Simon",            "Brian",            "Kevin",            "Steve",            "Kenedy",            "Harry",            "Japeth",            "Jack",            "Anthony",            "Bailey",            "Billy",            "Archie",            "Alexander",            "Adam",            "Benjamin",            "Charles",            "Daniel",            "David",            "Edward",            "Dominic",            "George",            "Harvey",            "Harrison",            "Jonathan",            "John",            "Joseph",            "Jacob",            "Isaac",            "Henry",            "Harry",            "James",            "Joshua",            "Matthew",            "Michael",            "Lucas"])
		last_name = random.choice([           "Njonjo",            "Nganga",            "Omollo",            "Kamau",            "Muema",            "Mwangi",            "Maina",            "Omundu",            "Njoroge",            "Wandoto",            "Kibe",            "Wachioi",            "Nyagah",            "Mwaura",            "Kamau",            "Mutiso",            "Ngungu",            "Mutali",            "Maina",            "Nganga",            "Mulinge",            "Thuku",            "Muhia",            "Mulinge",            "Gachanga",            "Chege",            "Chilemba",            "Chitundu",            "Chiumbo",            "Gachora",            "Gacoki",            "Gakere",            "Gakuru",            "Macaria ",            "Gatete",            "Gathee",            "Maina",            "Maitho",            "Makalani",            "Matunde",            "Mbogo",            "Maundu",            "Miano",            "Mathaathi",            "Matu",            "Migwi",            "Mpenda",            "Morani",            "Mugi",            "Muga",            "Mukiri",            "Muiru",            "Mukanda",            "Mugo",            "Muiruri",            "Mbui",            "Makori",            "Muenda",            "Mugendi","Chege ","Chomba ","Ciugaua","Gichere","Gachagua","Gachanja","Gachara","Gachii","Gakure","Gathaiya","Gathanja","Gathenya","Gathigira","Gathogo","Gathongo","Gathua","Gathuuri","Gakari","Karimi","Gitonga","Kimani","Kinuthia","Kenyatta ","Karanja","Kamotho","Kamande","Kagwa"])
		player[i] = Player.create(
									fname = first_name,lname = last_name,
									team = team ,
									weight = rand(76,136),
									height = rand(178,204),
									fitness = rand(min_seed,max_seed),
									stamina = rand(min_seed,max_seed),
									handling = rand(min_seed,max_seed),
									attack = rand(min_seed,max_seed),
									defense =rand(min_seed,max_seed),
									strength = rand(min_seed,max_seed),
									jumping = rand(min_seed,max_seed),
									speed = rand(min_seed,max_seed),
									technique =rand(min_seed,max_seed),
									agility = rand(min_seed,max_seed),
									kicking = rand(min_seed,max_seed),
									form = rand(1,100),
									aggression = rand(1,11),
									discipline = rand(1,11),
									energy = rand(1,100),
									leadership = rand(1,11),
									experience = rand(1,11),
									injured = False
								)



	print "The team was successful created. Division seed was: "+str(division)
	raw_input()
