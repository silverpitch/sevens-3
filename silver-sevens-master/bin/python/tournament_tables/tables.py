from peewee import *


db = mysql_db = MySQLDatabase('sevens',user='root',password='')

class Pools(Model):
	name = CharField()
	series= CharField()
	team_one = IntegerField()
	team_two = IntegerField()
	team_three = IntegerField()
	team_four = IntegerField()
	fixture_id = IntegerField()


	class Meta:
		database = db

class Pool_results(Model):
	team_one = IntegerField()
	team_two = IntegerField()
	pool = CharField()
	series = CharField()
	team_one_score = IntegerField()
	team_two_score = IntegerField()
	team_one_goal_difference = IntegerField()
	team_two_goal_difference = IntegerField()
	team_one_points = IntegerField()
	team_two_points = IntegerField()
	fixture_stats = IntegerField()

	class Meta:
		database = db


class playoffs(Model):
	team_one= IntegerField()
	team_two= IntegerField()
	stage = CharField()
	match_type = CharField()
	fixture_id= IntegerField()

	class Meta:
		database = db
	

class playoff_results(Model):
	team_one= IntegerField()
	team_two = CharField()
	stage = CharField()
	match_type = CharField()
	winner= IntegerField()
	fixture_stats= IntegerField()

	class Meta:
		database = db	
