from peewee import *
from db_tables.tables import *
from tournament_tables.tables import *
from libs.general import *
import time
import time
import sys

def progres_bar():
    for i in range(11):
        time.sleep(1)
        n = i *10
        sys.stdout.write("\r%d%%" % n)
        sys.stdout.flush()
try:
    db.create_tables([Pools,Pool_results,playoffs,playoff_results])
    progres_bar()
    print ' Tournament Tables were created successfully... :)'
except OperationalError:
    progres_bar()
    print ' Sorry, Tournament Tables already exist.'
 


try:
    db.create_tables([Team,Player,Fixtures,Fixture_Statistics,Commentary])
    progres_bar()
    print ' Tables were created successfully.'
    raw_input()
except OperationalError:
    progres_bar()
    print ' Tables already exist.'
    raw_input()