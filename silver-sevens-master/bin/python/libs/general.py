import random


def rand(x,y):
	if isinstance(x, int) == False or isinstance(y, int) == False:		#checks to see if the values passed in are intergers
		return 'Value '+str(x)+' and '+str(y) + ' must be integers.'
	elif y <= x:														#checks to see if x is greater than y
		return 'Value '+str(x)+' must be less than value '+str(y)
	else:
		value = random.randrange(x,y)
		return value;


def show_result(x):
	print x


