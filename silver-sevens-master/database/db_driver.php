<?php

include('config.php');


function connect() {
    $connect = mysql_connect("127.0.0.1", "root", "");
    if (!$connect) {
        die(mysql_error());
    }
    $dbconnection = mysql_select_db('sevens');
    if (!$dbconnection) {
        die(mysql_error());
    }
}


//access the player ids of the home team.
function get_home_team_players($id) {
	//start by querying the database to get the fixture details(because this has to be done)
	$query = mysql_query("SELECT * FROM fixtures WHERE id='$id'");

    if (!$query) {
        die(mysql_error());
    }
    $row = mysql_fetch_assoc($query);

    if (!$row) {
        die("<br><p><div align=\"center\"><h1>Match ID: $id could not be found.</div></h1>");
    }

	$prop_one = $row['p1t1'];  
	$hooker = $row['p2t1']; 
	$prop_two = $row['p3t1']; 
	$scrum_half = $row['p4t1']; 
	$fly_half = $row['p5t1']; 
	$centre = $row['p6t1']; 
	$fullback = $row['p7t1'];
	$sub_one = $row['p8t1'];
	$sub_two = $row['p9t1'];
	$sub_three = $row['p10t1'];
	$sub_four =  $row['p11t1'];
	$sub_five =  $row['p12t1'];
	$home_team =  $row['home_team_id'];
    $team_one_attack = $row['team_one_attack'];
    $team_one_defense = $row['team_one_defense'];


    return array($prop_one,$prop_two,$hooker,$scrum_half,$fly_half,$centre,$fullback,$home_team,$team_one_attack,$team_one_defense);
}


//function that access the players(id numbers) of the second team that is the guest team.
function get_guest_team_players($id) {
	//start by querying the database to get the fixture details(because this has to be done)
	$query = mysql_query("SELECT * FROM fixtures WHERE id='$id'");

    if (!$query) {
        die(mysql_error());
    }
    $row = mysql_fetch_assoc($query);

    if (!$row) {
        die("<br><p><div align=\"center\"><h1>Match ID: $id could not be found.</div></h1>");
    }

	$prop_one = $row['p1t2'];  
	$hooker = $row['p2t2']; 
	$prop_two = $row['p3t2']; 
	$scrum_half = $row['p4t2']; 
	$fly_half = $row['p5t2']; 
	$centre = $row['p6t2']; 
	$fullback = $row['p7t2'];
	$sub_one = $row['p8t2'];
	$sub_two = $row['p9t2'];
	$sub_three = $row['p10t2'];
	$sub_four =  $row['p11t2'];
	$sub_five =  $row['p12t2'];
	$guest_team =  $row['guest_team_id'];
    $team_two_attack = $row['team_two_attack'];
    $team_two_defense = $row['team_two_defense'];
    return array($prop_one,$prop_two,$hooker,$scrum_half,$fly_half,$centre,$fullback,$guest_team,$team_two_attack,$team_two_defense);
}


function get_player_details($team_id, $player_id) {
	//start by querying the database to get the fixture details(because of this has to be done)
	$query = mysql_query("SELECT * FROM player WHERE id='$player_id' AND team_id = '$team_id'");

    if (!$query) {
        die(mysql_error());
    }
    $row = mysql_fetch_row($query);

    if (!$row) {
        die(mysql_error());
    }

    //$value = array(1)
    $id = $row[0];
    $name = $row[1];
    $sname = $row[2];
	$fitness = $row[6];
	$handling = $row[8];
	$attack = $row[9];
	$tackling = $row[10];
	$speed = $row[13];
	$technique = $row[15];
	$strength = $row[11];
	$kicking = $row[16];
	$fatigue = $row[20];
	$conditioning = $row[19];
	$weight = $row[4];
	$tries = 0;
	$errors = 0;
	$energy = $row[21];
	$agility = $row[17];
 return array(
			$name, 
			$fitness, 
			$handling, 
			$attack,
			$tackling, 
			$speed,
			$technique,
			$strength,
			$kicking, 
			$fatigue,
			$conditioning,
			$weight,
			$tries, 
			$errors,
			$errors,
			$sname,
			$energy,
			$agility,
			$id,
 	     );
}

function get_teamname($team_id){
$query = mysql_query("SELECT * FROM team WHERE id='$team_id'");

    if (!$query) {
        die(mysql_error());
    }
    $row = mysql_fetch_row($query);

    if (!$row) {
        die(mysql_error());
    }

	$name =$row['1'];

	return $name;
	}


function update_players($id,$team_number,$player_number){
	global $player;


	$fitness = $player[$team_number][$player_number][1];
	$handling = $player[$team_number][$player_number][2];
	$attack = $player[$team_number][$player_number][3];
	$tackling = $player[$team_number][$player_number][4]; 
	$speed = $player[$team_number][$player_number][5];
	$technique = $player[$team_number][$player_number][6];
	$strength = $player[$team_number][$player_number][7];
	$kicking = $player[$team_number][$player_number][8];
	$energy = $player[$team_number][$player_number][16];
	

	$query = mysql_query("UPDATE `player` SET `fitness`='$fitness',
	`handling`='$handling',
	`attack`='$attack',
	`defense`='$tackling',
	`strength`='$strength',
	`speed`='$speed',
	`technique`='$technique',
	`kicking`='$kicking',
	`energy`='$energy'
	 WHERE id = '$id'");



	     if (!$query) {
        die(mysql_error());
    }
}

function save_commentary($string){

	global $match_id;

	$query =  mysql_query("INSERT INTO `commentary`( `fixture_id`, `commentary`) VALUES ('$match_id','$string')");

	echo $string;
	     if (!$query) {
        die(mysql_error());
}
}



function save_statistics(){
		global $player, $ground, $gain, $minutes, $mauls_won,$rucks_won,$scrums_won,$errors01,$errors02,$stat_tries1,$stat_tries2,$match_id,$try;
		global $current, $number, $opponent, $team,$points;
		global $tendancy_to_pass, $ruck_commit, $attack_method, $defence_method;

$home_team_conversions=$points[1][1];
$home_team_total_points = $try[1];
$home_team_mauls_won = $mauls_won[1];
$home_team_possesion = 50;
$home_team_scrum_won = $scrums_won[1];
$home_team_errors = $errors01;
$home_team_rucks_won = $rucks_won[1];

$guest_team_conversions=$points[2][1];
$guest_team_total_points = $try[2];
$guest_team_mauls_won = $mauls_won[2];
$guest_team_possesion = 50;
$guest_team_scrum_won = $scrums_won[2];
$guest_team_errors = $errors01;
$guest_team_rucks_won = $rucks_won[2];




	$query = mysql_query("INSERT INTO `fixture_statistics`(`fixture_id`,
	 `home_team_tries`, 
	 `home_team_conversions`,
		`home_team_total_points`,
		 `home_team_rucks_won`, 
		 `home_team_mauls_won`,
		 `home_team_possesion`, 
		`home_team_scrum_won`, 
		`home_team_errors`,

		 `guest_team_tries`,
		  `guest_team_conversions`,
		 `gueste_team_total_points`, 
		 `guest_team_rucks_won`, 
		 `guest_team_mauls_won`,
		 `guest_team_possesion`, 
		 `guest_team_scrum_won`,
		  `guest_team_errors`) VALUES 
		('$match_id',
			'$stat_tries1',
			'$home_team_conversions',
			'$home_team_total_points',
		'$home_team_rucks_won',
		'$home_team_mauls_won','$home_team_possesion','$home_team_scrum_won','$home_team_errors',
			'$stat_tries2',
			'$guest_team_conversions',
			'$guest_team_total_points',
		'$guest_team_rucks_won',
		'$guest_team_mauls_won','$guest_team_possesion','$guest_team_scrum_won','$guest_team_errors')
		");

			     if (!$query) {
        die(mysql_error());
}

	$query = mysql_query("UPDATE `fixtures` SET `played`='1' WHERE id = '$match_id'");

			     if (!$query) {
        die(mysql_error());
}

}
?>