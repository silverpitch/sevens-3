<?php
	function check_handling()
	{
		global $player;
		global $current, $number;
		/* handling is first of all influenced by two decisions, either they can knock or not. That is a 50-50 chance */
		$player_handling = 50;
		//the other 50 is gotten from the players value out of 25 converted to out of 50, that is *2
		$player_handling = $player_handling + ($player[$current][$number][2] * 4.5);
		//the higher the value of the skill, the less the chance he will knock
		$handling_chance = mt_rand(1, 100);
		
		if ($handling_chance <= $player_handling)
		{
			return $result = TRUE;
		}
		else
		{
			/*at times the ref does not notice a knock on, or the ref calls an advantage or simply favours one team
				so the call is not made. Taking this factors into account, 60% of the time the ref will call while 40%
				of the time he will ignore. */
			
			if (mt_rand(1,100)<=40)
			{
				return $result = FALSE;
			}
			else
			{
				return $result = TRUE;
			}

		}

	}

	
	function check_fitness ($players_number,$player_team)
	{
		global $player;
		$speed_of_player=($player[$player_team][$players_number][5]*100)/25;
		$fitness_chance=mt_rand(1,100);
		
		if ($fitness_chance<=$speed_of_player)
		{
			//player's speed is to be increased from anywhere between 100 and 125
			$new_speed=(mt_rand(100,125)/100)*20;
			return $new_speed;
		}
		else
		{
			//fitness does not check out therefore
			$new_speed=(mt_rand(75,100)/100)*20;
			return $new_speed;
		}

	}


	?>