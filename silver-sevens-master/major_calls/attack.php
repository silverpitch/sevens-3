<?php
	//attack function for backs
	function run_backs()
	{
		global $player, $ground, $gain, $minutes, $try,$energy;
		global $current, $number, $opponent, $team;
		global $tendancy_to_pass, $ruck_commit, $attack_method, $defence_method;
		$opponent_player = mt_rand(0, 4);
		//random defender is selected
		$opponent_player2 = $opponent_player + 1;
		$opponent_player3 = $opponent_player + 2;
		// now we check for tactics
		$ground = round($ground);
		save_commentary($player[$current][$number][0] . " goes in for the attack at <i>$ground metres</i>. ");
		
		if ($attack_method[$current] == "aggressive" && $defence_method[$opponent] == "wide")
		{
			/* wide defence means players are spread across the field. so likely-hood of encountering
					one defender is high,while aggressive attack means the main skills that are to be tested are
					strength, fitness and endurance */			//random defender is selected
			$total_strength = $player[$current][$number][7] + $player[$opponent][$opponent_player][7];
			$current_strength = ($player[$current][$number][7] * 100) / $total_strength;
			
			if (mt_rand(1, 100) <= $current_strength)
			{
				// the higher the strength over their opponent, the higher the chance of breaking through,hence the random generator
				save_commentary($player[$current][$number][0] . " goes over " . $player[$opponent][$opponent_player][0] . " like a truck. ");
				$defenders = 2;
				line_break($defenders);
			}
			else
			{
				
				if (mt_rand(1,100)<=mt_rand(40,49))
				{
					
					if (check_energy())
					{
						save_commentary($player[$current][$number][0] . " seemingly pumped up goes over " . $player[$opponent][$opponent_player][0] . " like a truck. ");
						$defenders = 2;
						line_break($defenders);
					}
					else
					{
						save_commentary($player[$current][$number][0] . " is immediatley stopped by the defenders. <" . $player[$opponent][$opponent_player][0] . "
						just could not let him pass,great confidence and skill . ");
						call_maul();
					}

				}
				else
				{
					save_commentary($player[$current][$number][0] . " is immediatley stopped by the defenders. <" . $player[$opponent][$opponent_player][0] . "
						just could not let him pass,great confidence and skill . ");
					call_maul();
				}

			}

		}

		elseif ($attack_method[$current] == "aggressive" && $defence_method[$opponent] == "group")
		{
			
			if (mt_rand(2, 3) == 2)
			{
				//random defender is selected
				$total_strength = $player[$current][$number][7] + $player[$opponent][$opponent_player][7] + $player[$opponent][$opponent_player2][7];
				$current_strength = ($player[$current][$number][7] * 100) / $total_strength;
				
				if (mt_rand(1, 100) <= $current_strength)
				{
					// the higher the strength over their opponent, the higher the chance of breaking through,hence the random generator
					save_commentary($player[$current][$number][0] . " has displayed great strenghth there as he knocks off two defenders. ");
					line_break(1);
				}
				else
				{
					
					if (mt_rand(1,100)<=mt_rand(40,49))
					{
						
						if (check_energy())
						{
							save_commentary($player[$current][$number][0] . " has displayed great strenghth  and energy there as he knocks off two defenders effortlessly. ");
							line_break(1);
						}
						else
						{
							save_commentary($player[$current][$number][0] . " is immediatley stopped by " . $player[$opponent][$opponent_player][0] . " and " . $player[$opponent][$opponent_player2][0]);
							call_maul();
						}

					}
					else
					{
						save_commentary($player[$current][$number][0] . " is immediatley stopped by " . $player[$opponent][$opponent_player][0] . " and " . $player[$opponent][$opponent_player2][0]);
						call_maul();
					}

				}

			}
			else
			{
				// *******************************************************************************
				// *******************************************************************************
				// A repetition of the above function only difference is defenders involved are three.
				$total_strength = $player[$current][$number][7] + $player[$opponent][$opponent_player][7] + $player[$opponent][$opponent_player2][7] + $player[$opponent][$opponent_player3][7];
				$current_strength = ($player[$current][$number][7] * 100) / $total_strength;
				
				if (mt_rand(1, 100) <= $current_strength)
				{
					// the higher the strength over their opponent, the higher the chance of breaking through,hence the random generator
					save_commentary($player[$current][$number][0] . " has displayed great strenghth there as he knocks off three defenders. ");
					line_break(1);
				}
				else
				{
					
					if (mt_rand(1,100)<=mt_rand(40,49))
					{
						
						if (check_energy())
						{
							// the higher the strength over their opponent, the higher the chance of breaking through,hence the random generator
							save_commentary($player[$current][$number][0] . " has displayed great strenghth and energy there as he knocks off three defenders. ");
							line_break(1);
						}
						else
						{
							//mt_rand( 1, 100 ) <= $current_strength else {
							save_commentary($player[$current][$number][0] . " is immediatley stopped by the combined efforts of  " . $player[$opponent][$opponent_player][0] . "</b> and <b>" . $player[$opponent][$opponent_player3][0]."</b>. Looks like a maul is about to be formed at
							<i>$ground metres.</i>");
							call_maul();
						}

					}
					else
					{
						//mt_rand( 1, 100 ) <= $current_strength else {
						save_commentary($player[$current][$number][0] . " is immediatley stopped by the combined efforts of  " . $player[$opponent][$opponent_player][0] . "</b> and <b>" . $player[$opponent][$opponent_player3][0]."</b>. Looks like a maul is about to be formed at
							<i>$ground metres.</i>");
						call_maul();
					}

				}

				// a maul is called
			}

			// check elapsed time
			// check_time();
			// *******************************************************************************
			// *******************************************************************************
		}

		elseif ($attack_method[$current] == "technical" && $defence_method[$opponent] == "wide")
		{
			/* wide defence means players are spread across the field. so likely-hood of encountering
					one defender is high,while aggressive attack means the main skills that are to be tested are
					strength, fitness and endurance */			//random defender is selected
			$total_value = $player[$current][$number][3] + $player[$opponent][$opponent_player][4];
			$current_value = ($player[$current][$number][4] * 100) / $total_value;
			
			if (mt_rand(1, 100) <= $current_value)
			{
				// the player has managed to over-come the tackle hence a line break
				save_commentary($player[$current][$number][0] . " out smarts " . $player[$opponent][$opponent_player][0] . " with a dummy to the left!!!  ");
				$defenders = 2;
				line_break($defenders);
			}
			else
			{
				save_commentary("but " . $player[$current][$number][0] . " is quickly brought down to a halt by " . $player[$opponent][$opponent_player][0] . "</b>, what a brilliant tackle! A ruck is formed.");
				call_ruck();
			}

			
			if (mt_rand(1,100)<=mt_rand(40,49))
			{
				
				if (check_energy())
				{
					// the higher the strength over their opponent, the higher the chance of breaking through,hence the random generator
					save_commentary($player[$current][$number][0] . " has displayed great strenghth and energy there as he knocks off three defenders. ");
					line_break(1);
				}
				else
				{
					//mt_rand( 1, 100 ) <= $current_strength else {
					save_commentary($player[$current][$number][0] . " is immediatley stopped by the combined efforts of  " . $player[$opponent][$opponent_player][0] . "</b> and <b>" . $player[$opponent][$opponent_player3][0]."</b>. Looks like a maul is about to be formed at
							<i>$ground metres.</i>");
					call_maul();
				}

			}
			else
			{
				//mt_rand( 1, 100 ) <= $current_strength else {
				save_commentary($player[$current][$number][0] . " is immediatley stopped by the combined efforts of  " . $player[$opponent][$opponent_player][0] . "</b> and <b>" . $player[$opponent][$opponent_player3][0]."</b>. Looks like a maul is about to be formed at
							<i>$ground metres.</i>");
				call_maul();
			}

		}

		elseif ($attack_method[$current] == "technical" && $defence_method[$opponent] == "group")
		{
			
			if (mt_rand(2, 3) == 2)
			{
				//random defender is selected
				$total_strength = $player[$current][$number][3] + $player[$opponent][$opponent_player][4] + $player[$opponent][$opponent_player2][4];
				$current_strength = ($player[$current][$number][3] * 100) / $total_strength;
				
				if (mt_rand(1, 100) <= $current_strength)
				{
					// the higher the strength over their opponent, the higher the chance of breaking through,hence the random generator
					save_commentary($player[$current][$number][0] . " manuvuers brilliantly past " . $player[$opponent][$opponent_player][0] . " and " . $player[$opponent][$opponent_player2][0] . "</b>");
					line_break(1);
				}
				else
				{
					
					if (mt_rand(1,100)<=mt_rand(40,49))
					{
						
						if (check_energy())
						{
							// the higher the strength over their opponent, the higher the chance of breaking through,hence the random generator
							save_commentary($player[$current][$number][0] . " manuvuers brilliantly past " . $player[$opponent][$opponent_player][0] . " and " . $player[$opponent][$opponent_player2][0] . "</b>");
							line_break(1);
						}
						else
						{
							save_commentary($player[$current][$number][0] . " is immediatley tackled  " . $player[$opponent][$opponent_player][0] . " and " . $player[$opponent][$opponent_player2][0] . "</b>, the fans seem to have lost hope after that tackle.");
							call_maul();
							;
						}

					}
					else
					{
						//mt_rand( 1, 100 ) <= $current_strength else {
						save_commentary($player[$current][$number][0] . " is immediatley tackled  " . $player[$opponent][$opponent_player][0] . " and " . $player[$opponent][$opponent_player2][0] . "</b>, the fans seem to have lost hope after that tackle.");
						call_maul();
					}

					// a maul is called
				}

				// check elapsed time
				// check_time();
				/******************************************************************************************************************************************/				/* END OF AGGRESSIVE VS WIDE DEFENCE BRANCH OF THE FUNCTION */
			}
			else
			{
				// *******************************************************************************
				// *******************************************************************************
				// A repetition of the above function only difference is defenders involved are three.
				//random defender is selected
				$total_strength = $player[$current][$number][3] + $player[$opponent][$opponent_player][4] + $player[$opponent][$opponent_player2][4] + $player[$opponent][$opponent_player3][4];
				$current_strength = ($player[$current][$number][7] * 100) / $total_strength;
				
				if (mt_rand(1, 100) <= $current_strength)
				{
					// the higher the strength over their opponent, the higher the chance of breaking through,hence the random generator
					save_commentary($player[$current][$number][0] . " has displayed great skill by getting past one ... two wait three! defenders and is clear for the line! . ");
					line_break(1);
				}

				//mt_rand( 1, 100 ) <= $current_strength else {
				save_commentary($player[$current][$number][0] . " is immediatley stopped by " . $player[$opponent][$opponent_player][0] . "," . $player[$opponent][$opponent_player3][0] . " and " . $player[$opponent][$opponent_player2][0]);
				call_maul();
				// a maul is called
			}

		}

	}

	
	function run_winger()
	{
		global $player, $ground, $gain, $minutes, $try;
		global $current, $number, $opponent, $team;
		global $tendancy_to_pass, $ruck_commit, $attack_method, $defence_method;
		/*player is a winger. Now we all now wingers tend to do their own thing in that they dont pass ball that much
or play according to tactics that much*/		// first of all depending on tactics he will either try to play in line with the forwards or with the backs]
		$next_move = mt_rand(1, 100);
		
		if ($next_move <= 55)
		{
			// player to follow tactics but strength will not be tested in-case he player to to the forwards
			
			if ($attack_method[$current] == "aggressive" && $defence_method[$opponent] == "wide")
			{
				/* wide defence means players are spread across the field. so likely-hood of encountering
					one defender is high,while aggressive attack means the main skills that are to be tested are
					strength, fitness and endurance */
				$opponent_player = mt_rand(0, 6);
				//random defender is selected
				$total_strength = $player[$current][$number][7] + $player[$opponent][$opponent_player][7];
				$current_strength = ($player[$current][$number][7] * 100) / $total_strength;
				
				if (mt_rand(1, 100) <= $current_strength)
				{
					// the higher the strength over their opponent, the higher the chance of breaking through,hence the random generator
					save_commentary($player[$current][$number][0] . " being a winger, tramples over" . $player[$opponent][$opponent_player][0] . "</b> with such minimum effort. ");
					line_break(2);
				}
				else
				{
					save_commentary($player[$current][$number][0] . " is immediatley stopped by the defenders." . $player[$opponent][$opponent_player][0] . "</b> did a good job to stop that train. ");
					call_maul();
					// a maul is called instead of a ruck
				}

			}

			elseif ($attack_method[$current] == "aggressive" && $defence_method[$opponent] == "group")
			{
				$opponent_player = mt_rand(0, 4);
				//random defender is selected
				$opponent_player2 = $opponent_player + 1;
				$opponent_player3 = $opponent_player + 2;
				$total_strength = $player[$current][$number][7] + $player[$opponent][$opponent_player][7] + $player[$opponent][$opponent_player2][7] + $player[$opponent][$opponent_player3][7];
				$current_strength = ($player[$current][$number][7] * 100) / $total_strength;
				
				if (mt_rand(1, 100) <= $current_strength)
				{
					// the higher the strength over their opponent, the higher the chance of breaking through,hence the random generator
					save_commentary($player[$current][$number][0] . " has displayed great strenghth there as he knocks off three defenders. ");
					line_break(1);
				}
				else
				{
					save_commentary($player[$current][$number][0] . " is immediatley stopped by " . $player[$opponent][$opponent_player][0] . " and " . $player[$opponent][$opponent_player3][0]);
					call_maul();
					// a maul is called
				}

			}

			elseif ($attack_method[$current] == "technical" && $defence_method[$opponent] == "wide")
			{
				/* wide defence means players are spread across the field. so likely-hood of encountering
					one defender is high,while aggressive attack means the main skills that are to be tested are
					strength, fitness and endurance */
				$opponent_player = mt_rand(0, 6);
				//random defender is selected
				$total_value = $player[$current][$number][3] + $player[$opponent][$opponent_player][4];
				$current_value = ($player[$current][$number][4] * 100) / $total_value;
				
				if (mt_rand(1, 100) <= $current_value)
				{
					// the player has managed to over-come the tackle hence a line break
					save_commentary($player[$current][$number][0] . " out smarts " . $player[$opponent][$opponent_player][0] . " with a dummy to the left!!!  ");
					$defenders = 2;
					line_break($defenders);
				}
				else
				{
					save_commentary("but " . $player[$current][$number][0] . " is quickly brought down to a halt by " . $player[$opponent][$opponent_player][0] . "</b>, what a brilliant tackle! A ruck is formed.");
					call_ruck();
				}

			}

			elseif ($attack_method[$current] == "technical" && $defence_method[$opponent] == "group")
			{
				
				if (mt_rand(2, 3) == 2)
				{
					$opponent_player = mt_rand(0, 5);
					//random defender is selected
					$opponent_player2 = $opponent_player + 1;
					$total_strength = $player[$current][$number][3] + $player[$opponent][$opponent_player][4] + $player[$opponent][$opponent_player2][4];
					$current_strength = ($player[$current][$number][3] * 100) / $total_strength;
					
					if (mt_rand(1, 100) <= $current_strength)
					{
						// the higher the strength over their opponent, the higher the chance of breaking through,hence the random generator
						save_commentary($player[$current][$number][0] . " manuvuers brilliantly past <b>" . $player[$opponent][$opponent_player][0] . "</b> and <b>" . $player[$opponent][$opponent_player2][0] . "</b>");
						line_break(1);
					}
					else
					{
						save_commentary($player[$current][$number][0] . " is immediatley tackled <b>" . $player[$opponent][$opponent_player][0] . "</b> and <b>" . $player[$opponent][$opponent_player2][0] . "</b>, the fans seem to have lost hope after that tackle.");
						call_maul();
						// a maul is called
					}

					// check elapsed time
					// check_time();
					/******************************************************************************************************************************************/					/* END OF AGGRESSIVE VS WIDE DEFENCE BRANCH OF THE FUNCTION */
				}
				else
				{
					// *******************************************************************************
					// *******************************************************************************
					// A repetition of the above function only difference is defenders involved are three.
					$opponent_player = mt_rand(0, 4);
					//random defender is selected
					$opponent_player2 = $opponent_player + 1;
					$opponent_player3 = $opponent_player + 2;
					$total_strength = $player[$current][$number][3] + $player[$opponent][$opponent_player][4] + $player[$opponent][$opponent_player2][4] + $player[$opponent][$opponent_player3][4];
					$current_strength = ($player[$current][$number][7] * 100) / $total_strength;
					
					if (mt_rand(1, 100) <= $current_strength)
					{
						// the higher the strength over their opponent, the higher the chance of breaking through,hence the random generator
						save_commentary($player[$current][$number][0] . " has displayed great skill by getting past one ... two wait three! defenders and is clear for the line! . ");
						line_break(1);
					}

					//mt_rand( 1, 100 ) <= $current_strength else {
					save_commentary($player[$current][$number][0] . " is immediatley stopped by " . $player[$opponent][$opponent_player][0] . "," . $player[$opponent][$opponent_player3][0] . " and " . $player[$opponent][$opponent_player2][0]);
					call_maul();
					// a maul is called
				}

			}

		}
		else
		{
			// the winger is to follow his own intuition that is try and out run the players
			// he can choose to either try and out with and out run the defenders or he can choose to use strength
			// 65% of the time he will try and out run the defender
			$opponent_player = mt_rand(0, 5);
			$defender = $opponent_player;
			
			if (mt_rand(1, 100) <= 65)
			{
				
				if (!(($ground - 22) <= 0) || !(($ground + 22) >= 100))
				{
					// things get ugly again
					// the player is within the 22
					$attackers_ground = $ground;
					
					if ($current == 1)
					{
						$defenders_ground = $ground + 5;
					}

					//$current == 1 else {
					$defenders_ground = $ground - 5;
				}

				$relative_ground = $attackers_ground - $defenders_ground;
				$relative_ground = abs($relative_ground);
				$attacker_speed = ($player[$current][$number][5] * (check_fitness($number, $current))) / 10;
				$defender_speed = ($player[$opponent][$defender][5] * (check_fitness($defender, $opponent))) / 10;
				$relative_speed = $attacker_speed + $defender_speed;
				// time taken for the defender to reach the attacker
				$time = $relative_ground / $relative_speed;
				// distance gained
				$gain = $attacker_speed * $time;
				$gain = round($gain);
				save_commentary(". He runs $gain metres before " . $player[$opponent][$defender][0] . " cathes up with him! What a great winger there!. ");
				// the next decision is randomly decided i.e. if the player will try to over power the tackle or he will try and escape the tackle
				
				if (mt_rand(1, 2) == 1)
				{
					// player will use strength to out power the tackle
					$total = $player[$opponent][$defender][4] + $player[$current][$number][7];
					
					if (mt_rand(1, 100) <= ($player[$current][$number][7] * 100) / $total)
					{
						// player over powers his tackler and makes a run for the posts
						save_commentary(" and he breaks off that line of defenders, he still has more challeges to over come. ");
						line_break(2);
					}
					else
					{
						// he is brought down.
						// gain is determined by players endurance
						$gain = (mt_rand(145, mt_rand(199, 215))) / 100 * $player[$current][$number][6];
						$gain = round($gain);
						determine_side();
						$ground = round($ground);
						save_commentary(" He gains $gain  metres before he is brought down at <i>$ground metres</i>. Players from both sides hurry in for a maul.");
						call_maul();
					}

				}
				else
				{
					// player will try and evade the tackle
					$total = $player[$opponent][$defender][4] + $player[$current][$number][3];
					
					if (mt_rand(1, 100) <= ($player[$current][$number][3] * 100) / $total)
					{
						// player over powers his tackler and makes a run for the posts
						save_commentary(" he brilliantly side steps " . $player[$opponent][$defender][0] . " to make a break on the defence line. ");
						$defenders = 2;
						line_break($defenders);
					}
					else
					{
						save_commentary(" . The winger is immediately brought down to a halt. ");
						call_ruck();
					}

				}

			}
			else
			{
				// he will solely try and use strength to over power the defender
				$total = $player[$opponent][$defender][4] + $player[$current][$number][7];
				
				if (mt_rand(1, 100) <= ($player[$current][$number][7] * 100) / $total)
				{
					// player over powers his tackler and makes a run for the posts
					save_commentary(" and he breaks off that line of defenders, he still has more challeges to over come. ");
					// line_break(1);
					// check what the opponents are playing before calling a line break
					
					if ($attack_method[$current] == "aggressive" && $defence_method[$opponent] == "group")
					{
						line_break(1);
					}
					else
					{
						line_break(2);
					}

				}
				else
				{
					// he is brought down.
					// gain is determined by players endurance
					$gain = (mt_rand(125, mt_rand(199, 225))) / 100 * $player[$current][$number][6];
					$gain = round($gain);
					determine_side();
					$ground = round($ground);
					save_commentary(" He gains $gain  metres before he is brought down at <i>$ground metres</i>. Players from both sides hurry in for a maul.");
					call_maul();
				}

			}

		}

	}

	
	function run_forwards()
	{
		global $player, $ground, $gain, $minutes, $try,$energy;
		global $current, $number, $opponent, $team;
		global $tendancy_to_pass, $ruck_commit, $attack_method, $defence_method;
		$ground = round($ground);
		/********************************************************************************************************************/		//player is not to pass and moves to set up a maul/ATTACK
		save_commentary($player[ $current ][ $number ][ 0 ] . " goes in for the attack at <i>$ground metres</i>. ");
		
		if ( $attack_method[ $current ] == "aggressive" && $defence_method[ $opponent ] == "wide" ) //checking tactics to determine the next move
		
			{
			/* wide defence means players are spread across the field. so likely-hood of encountering
                one defender is high,while aggressive attack means the main skills that are to be tested are
                strength, fitness and endurance */
			$opponent_player = mt_rand( 0, 6 );
			//random defender is selected
			$total_strength = $player[ $current ][ $number ][ 7 ] + $player[ $opponent ][ $opponent_player ][ 7 ];
			$current_strength = ( $player[ $current ][ $number ][ 7 ] * 100 ) / $total_strength;
			
			if ( mt_rand( 1, 100) <= $current_strength )
			{
				//the higher the strength over their opponent, the higher the chance of breaking through,hence the random generator
				save_commentary($player[ $current ][ $number ][ 0 ] . " goes over " . $player[ $opponent ][ $opponent_player ][ 0 ] . " like a with very mimumum effor. A show of great strength. ");
				//player broke out but since opposition did not play a closed up defence, more players are available stop the line break 
				$defenders = 2;
				line_break($defenders);
			}
			else
			{
				//calls for a maul
				
				if (mt_rand(1,100)<=mt_rand(40,49))
				{
					
					if (check_energy())
					{
						save_commentary($player[ $current ][ $number ][ 0 ] . " goes over " . $player[ $opponent ][ $opponent_player ][ 0 ] . " like a with very mimumum effor. A show of great strength. ");
						//player broke out but since opposition did not play a closed up defence, more players are available stop the line break 
						$defenders = 2;
						line_break($defenders);
					}
					else
					{
						save_commentary($player[ $current ][ $number ][ 0 ] . " is immediatley stopped by the defenders." . $player[ $opponent ][ $opponent_player ][ 0 ] . "</b> did a good job to stop that train. ");
						call_maul();
					}

				}
				else
				{
					save_commentary($player[ $current ][ $number ][ 0 ] . " is immediatley stopped by the defenders." . $player[ $opponent ][ $opponent_player ][ 0 ] . "</b> did a good job to stop that train. ");
					call_maul();
				}

			}

			//check elapsed time
			//check_time();
			//END OF FUNCTION BRANCH
		}

		elseif ( $attack_method[ $current ] == "aggressive" && $defence_method[ $opponent ] == "group" )
		{
			
			if ( mt_rand( 2, 3 ) == 2 )
			{
				$opponent_player = mt_rand( 0, 5 );
				//random defender is selected
				$opponent_player2 = $opponent_player + 1;
				$total_strength = $player[ $current ][ $number ][ 7 ] + $player[ $opponent ][ $opponent_player ][ 7 ] + $player[ $opponent ][ $opponent_player2 ][ 7 ];
				$current_strength = ( $player[ $current ][ $number ][ 7 ] * 100 ) / $total_strength;
				
				if ( mt_rand( 1, 100 ) <= $current_strength )
				{
					//the higher the strength over their opponent, the higher the chance of breaking through,hence the random generator
					save_commentary($player[ $current ][ $number ][ 0 ] . " has displayed great strenghth there as he knocks off two defenders. ");
					line_break(1);
				}
				else
				{
					save_commentary($player[ $current ][ $number ][ 0 ] . " is immediatley stopped by
										". $player[ $opponent ][ $opponent_player ][ 0 ] . " and " . $player[ $opponent ][ $opponent_player2 ][ 0 ]."</b>");
					call_maul();
					// a maul is called
					
					if (mt_rand(1,100)<=mt_rand(40,49))
					{
						
						if (check_energy())
						{
							//the higher the strength over their opponent, the higher the chance of breaking through,hence the random generator
							save_commentary($player[ $current ][ $number ][ 0 ] . " has displayed great strenghth there as he knocks off two defenders. ");
							line_break(1);
						}
						else
						{
							save_commentary($player[ $current ][ $number ][ 0 ] . " is immediatley stopped by
										". $player[ $opponent ][ $opponent_player ][ 0 ] . " and " . $player[ $opponent ][ $opponent_player2 ][ 0 ]."</b>");
							call_maul();
							// a maul is calle
						}

					}
					else
					{
						save_commentary($player[ $current ][ $number ][ 0 ] . " is immediatley stopped by
										". $player[ $opponent ][ $opponent_player ][ 0 ] . " and " . $player[ $opponent ][ $opponent_player2 ][ 0 ]."</b>");
						call_maul();
						// a maul is calle
					}

				}

				//check elapsed time
				//check_time();
				//END OF FUNCTION BRANCH
			}
			else
			{
				//*******************************************************************************
				//*******************************************************************************
				//A repetition of the above function only difference is defenders involved are three.
				$opponent_player = mt_rand( 0, 4 );
				//random defender is selected
				$opponent_player2 = $opponent_player + 1;
				$opponent_player3 = $opponent_player + 2;
				$total_strength = $player[ $current ][ $number ][ 7 ] + $player[ $opponent ][ $opponent_player ][ 7 ] + $player[ $opponent ][ $opponent_player2 ][ 7 ] + $player[ $opponent ][ $opponent_player3 ][ 7 ];
				$current_strength = ( $player[ $current ][ $number ][ 7 ] * 100 ) / $total_strength;
				
				if ( mt_rand( 1, 100 ) <= $current_strength )
				{
					//the higher the strength over their opponent, the higher the chance of breaking through,hence the random generator
					save_commentary($player[ $current ][ $number ][ 0 ] . " has displayed great strenghth there as he knocks off three defenders. ");
					line_break(1);
				}

				//mt_rand( 1, 100 ) <= $current_strength
				else
				{
					
					if (mt_rand(1,100)<=mt_rand(40,49))
					{
						
						if (check_energy())
						{
							//the higher the strength over their opponent, the higher the chance of breaking through,hence the random generator
							save_commentary($player[ $current ][ $number ][ 0 ] . " has displayed great strenghth there as he knocks off two defenders. ");
							line_break(1);
						}
						else
						{
							save_commentary($player[ $current ][ $number ][ 0 ] . " is immediatley stopped by
										" . $player[ $opponent ][ $opponent_player ][ 0 ] . " and " . $player[ $opponent ][ $opponent_player3 ][ 0]);
							call_maul();
							// a maul is cal
							// a maul is calle
						}

					}
					else
					{
						save_commentary($player[ $current ][ $number ][ 0 ] . " is immediatley stopped by
										" . $player[ $opponent ][ $opponent_player ][ 0 ] . " and " . $player[ $opponent ][ $opponent_player3 ][ 0 ]);
						call_maul();
						// a maul is cal
						// a maul is calle
					}

				}

				//check elapsed time
				//check_time();
				//*******************************************************************************
				//*******************************************************************************
			}

		}

		elseif ($attack_method[$current] == "technical" && $defence_method[$opponent] == "wide")
		{
			/* wide defence means players are spread across the field. so likely-hood of encountering
                one defender is high,while aggressive attack means the main skills that are to be tested are
                strength, fitness and endurance */
			$opponent_player = mt_rand(0, 6);
			//random defender is selected
			$total_value = $player[$current][$number][3] + $player[$opponent][$opponent_player][4];
			$current_value = ($player[$current][$number][4] * 100) / $total_value;
			
			if(mt_rand(1,2)==1)
			{
				$footing = "left";
			}
			else
			{
				$footing = "right";
			}

			
			if (mt_rand(1, 100) <= $current_value)
			{
				//the player has managed to over-come the tackle hence a line break
				save_commentary($player[$current][$number][0] . " out smarts " . $player[$opponent][$opponent_player][0] . " with a dummy to the $footing.  ");
				$defenders = 2;
				line_break($defenders);
			}
			else
			{
				save_commentary(" but " . $player[$current][$number][0] . " is quickly brought down to a halt by " . $player[$opponent][$opponent_player][0]."</b>, what a brilliant tackle! A ruck is formed.");
				call_ruck();
				
				if (mt_rand(1,100)<=mt_rand(40,49))
				{
					
					if (check_energy())
					{
						//the player has managed to over-come the tackle hence a line break
						save_commentary($player[$current][$number][0] . " out smarts " . $player[$opponent][$opponent_player][0] . " with a dummy to the $footing.  ");
						$defenders = 2;
						line_break($defenders);
					}
					else
					{
						save_commentary(" but " . $player[$current][$number][0] . " is quickly brought down to a halt by " . $player[$opponent][$opponent_player][0]."</b>, what a brilliant tackle! A ruck is formed.");
						call_ruck();
						// a maul is cal
						// a maul is calle
					}

				}
				else
				{
					save_commentary(" but " . $player[$current][$number][0] . " is quickly brought down to a halt by " . $player[$opponent][$opponent_player][0]."</b>, what a brilliant tackle! A ruck is formed.");
					call_ruck();
					// a maul is cal
					// a maul is calle
				}

			}

		}

		elseif ($attack_method[$current] == "technical" && $defence_method[$opponent] == "group")
		{
			
			if ( mt_rand( 2, 3 ) == 2 )
			{
				$opponent_player = mt_rand( 0, 5 );
				//random defender is selected
				$opponent_player2 = $opponent_player + 1;
				$total_strength = $player[ $current ][ $number ][ 3 ] + $player[ $opponent ][ $opponent_player ][ 4 ] + $player[ $opponent ][ $opponent_player2 ][ 4 ];
				$current_strength = ( $player[ $current ][ $number ][ 3 ] * 100 ) / $total_strength;
				
				if ( mt_rand( 1, 100 ) <= $current_strength )
				{
					//the higher the strength over their opponent, the higher the chance of breaking through,hence the random generator
					save_commentary($player[ $current ][ $number ][ 0 ] . " manuvuers brilliantly past ".$player[ $opponent ][ $opponent_player ][ 0 ]." and <b>".$player[ $opponent ][ $opponent_player2 ][ 0 ]."</b>");
					line_break(1);
				}
				else
				{
					
					if (mt_rand(1,100)<=mt_rand(40,49))
					{
						
						if (check_energy())
						{
							save_commentary($player[ $current ][ $number ][ 0 ] . " manuvuers brilliantly past ".$player[ $opponent ][ $opponent_player ][ 0 ]." and <b>".$player[ $opponent ][ $opponent_player2 ][ 0 ]."</b>");
							line_break(1);
							;
						}
						else
						{
							save_commentary($player[ $current ][ $number ][ 0 ] . " is immediatley tackled
										" . $player[ $opponent ][ $opponent_player ][ 0 ] . " and " . $player[ $opponent ][ $opponent_player2 ][ 0 ]."</b>, the fans seem to have lost hope after that tackle.");
							call_ruck();
						}

					}
					else
					{
						save_commentary($player[ $current ][ $number ][ 0 ] . " is immediatley tackled
										" . $player[ $opponent ][ $opponent_player ][ 0 ] . " and " . $player[ $opponent ][ $opponent_player2 ][ 0 ]."</b>, the fans seem to have lost hope after that tackle.");
						call_ruck();
					}

				}

				//check elapsed time
				//check_time();
				/******************************************************************************************************************************************/				/* END OF AGGRESSIVE VS WIDE DEFENCE BRANCH OF THE FUNCTION */
			}
			else
			{
				//*******************************************************************************
				//*******************************************************************************
				//A repetition of the above function only difference is defenders involved are three.
				$opponent_player = mt_rand( 0, 4 );
				//random defender is selected
				$opponent_player2 = $opponent_player + 1;
				$opponent_player3 = $opponent_player + 2;
				$total_strength = $player[ $current ][ $number ][ 3 ] + $player[ $opponent ][ $opponent_player ][ 4 ] + $player[ $opponent ][ $opponent_player2 ][ 4 ] + $player[ $opponent ][ $opponent_player3 ][ 4 ];
				$current_strength = ( $player[ $current ][ $number ][ 7 ] * 100 ) / $total_strength;
				
				if ( mt_rand( 1, 100 ) <= $current_strength )
				{
					//the higher the strength over their opponent, the higher the chance of breaking through,hence the random generator
					save_commentary($player[ $current ][ $number ][ 0 ] . " has displayed great skill by getting past one ... two wait three! defenders and is clear for the line! . ");
					line_break(1);
				}
				else
				{
					// a maul is called
					
					if (mt_rand(1,100)<=mt_rand(40,49))
					{
						
						if (check_energy())
						{
							//the higher the strength over their opponent, the higher the chance of breaking through,hence the random generator
							save_commentary($player[ $current ][ $number ][ 0 ] . " has displayed great skill by getting past one ... two wait three! defenders and is clear for the line! . ");
							line_break(1);
						}
						else
						{
							save_commentary($player[ $current ][ $number ][ 0 ] . " is immediatley stopped by
										" . $player[ $opponent ][ $opponent_player ][ 0 ] . "," . $player[ $opponent ][ $opponent_player3 ][ 0 ] ." and " . $player[ $opponent ][ $opponent_player2 ][ 0 ]);
							call_ruck();
						}

					}
					else
					{
						save_commentary($player[ $current ][ $number ][ 0 ] . " is immediatley stopped by
										" . $player[ $opponent ][ $opponent_player ][ 0 ] . "," . $player[ $opponent ][ $opponent_player3 ][ 0 ] ." and " . $player[ $opponent ][ $opponent_player2 ][ 0 ]);
						call_ruck();
					}

				}

			}

		}

	}

	?>